Title: Como contribuir um post para o blog
Date: 2020-08-06

Um dos principais objetivos desse blog é permitir que qualquer estudante da feec
possa escrever um post divulgando seu projeto.
Os artigos para o blog são escritos usando a sintaxe Markdown, e o site como um
todo é armazenado em um repositório git, hosteado no Gitlab.
Esse post visa explicar como lidar com essas duas ferramentas para permitir
a contribuição de mais pessoas interessadas.

Se você não quiser mexer com o git, pode seguir apenas a seção sobre Markdown e
nos enviar por um de [nossos grupos](https://hackerspacefeec.gitlab.io/salas-publicas-no-element-e-no-telegram.html) o arquivo .md para postarmos no blog.
Mas recomendamos aprender a usar o git por ser uma ferramenta extremamente
útil. Esse post mostra como usar a interface do gitlab para tal, mas caso
você tenha maior familiaridade com a ferramenta git, é perfeitamente possível
que o código seja escrito no seu editor de texto preferido.

Caso seu post possua bastante código e prefira escrever usando um [jupyter
notebok](https://jupyter.org/), também é possível!
Nosso site permite utilizar um jupyter notebook como post sem nenhuma
modificação.

## Markdown

A sintaxe do markdown utilizada para escrever os posts desse blog é bastante
simples e fácil de aprender.

Para criar um novo parágrafo, deixe uma linha em branco antes do texto do novo
parágrafo.

Para criar uma seção, escreva uma linha com dois `#` na frente.
Exemplo:
```md
## Minha seção
```

Uma subseção é criada utilizando `###`, subsubseção com `####`, e assim por
diante.

Apenas um `#` não é utilizado para criação de seção porque ele é usado no
título.
Por isso utiliza-se de `##` em diante.

Um texto pode ser escrito em *itálico* colocando `*` ou `_` em
volta:
```md
*Texto em itálico* e texto normal.
```

Para se escrever em **negrito**, utiliza-se dois `*` ou dois `_` em volta:
```md
**Texto em negrito** e texto normal.
```

Listas podem ser

* não-ordenadas
* ordenadas

Para listas não-ordenadas basta iniciar cada linha com `*` ou `-`, indentando
para obter sub-itens:
```md
* primeiro item
* segundo item
	* sub-item do segundo item
```

Já para listas ordenadas, inicie cada item com um número seguido de ponto (o
número não é relevante):
```md
1. primeiro item
2. segundo item
1. terceiro item, também funciona assim
1. quarto item
```

Um link é adicionado da seguinte forma:
```md
[Nome do link](https://url-do-link.com)
```

Imagens são adicionadas usando uma sintaxe similar, mas com um `!` na frente:
```md
![Título da imagem](caminho/para/imagem.png)
```

Código pode ser adicionado no meio do texto usando
    `
em volta dele:

```
`código` e texto normal.
```

Para blocos de código usa-se três
    `
seguidos para iniciar e outro para terminar o bloco de código. Adicionalmente pode-se
adicionar o nome da linguagem de programação, para que a marcação da linguagem seja correta:
```md
    ```python
    print("Código em python.")
    ```
```

Para mais informações sobre markdown:
[Mastering markdown](https://guides.github.com/features/mastering-markdown/)

Além de escrever seu artigo usando a sintaxe do markdown, é necessário também adicionar
```
Title: Como contribuir um post para o blog
Date: 2020-08-06
Author: seu nome
```
no **início** do arquivo com o título e data do seu artigo. A tag de autoria é opcional.

## Merge request

O git é uma ferramenta de versionamento de código e é quem está por trás do
funcionamento desse blog.
Por hoje vamos apenas mostrar o básico necessário para publicar o artigo usando
a interface do Gitlab, mas o funcionamento mais detalhado do git é bastante útil
de se saber e será tópico de um post futuro.

Após se registrar no gitlab, o primeiro passo é ir para o [repositório do site
hackerspace](https://gitlab.com/hackerspacefeec/hackerspacefeec.gitlab.io).
É aqui que ficam todos os arquivos que compõem o site, e uma mudança neles
implica em uma mudança no site.

Então é necessário criar um fork desse repositório clicando no botão "Fork":
![Botão de fork](images/gitlab-fork_button.png)

Isso criará uma cópia do repositório do site, da qual você é o dono e pode fazer
as modificações livremente nela.
Nesse seu repositório (note que essa é a sua cópia e não o original, como
destacado em azul) você pode clicar em "Web IDE", para fazer edições nos
arquivos pelo próprio navegador:
![Botão de Web IDE](images/gitlab-web_ide_button.png)

Navegue então para dentro da pasta `content` e então para `articles` e crie uma
pasta com o nome do tópico (apenas para ficar organizado), e dentro dela crie um
arquivo com extensão `.md`.
Dentro desse arquivo cole o conteúdo do artigo que você escreveu em Markdown,
conforme a seção anterior, e clique em "Commit...".

Então suas adições serão mostradas na direita.
No campo "Commit message" na esquerda você deve inserir
```
POST: Título do seu artigo
```
e clicar em "Commit".

Na tela seguinte, não é necessário fazer nenhuma modificação, apenas desça até o
final e clique em "Submit merge request".

Pronto!
Agora é só aguardar os outros membros revisarem o texto para fazer qualquer
correção que seja necessária, e quando seu *merge request* for aceito o post
automaticamente aparecerá no blog!

Para mais informações sobre merge requests:
[Creating merge requests](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)

Mais informações sobre o processo de criação de posts utilizando o Pelican podem ser conferidas em:
[Writing content](https://docs.getpelican.com/en/stable/content.html)

Qualquer dúvida sinta-se livre para pedir ajuda em [uma de nossas salas públicas](https://hackerspacefeec.gitlab.io/salas-publicas-no-element-e-no-telegram.html) ou até mesmo nos mandar um email (disponíveis na página "sobre").
