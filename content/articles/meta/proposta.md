Title: Proposta de criação de um hackerspace na FEEC
Date: 2019-04-04

---

Proposta inicial para a criação de um hackerspace na Faculdade de Engenharia Elétrica e de Computação (FEEC/Unicamp) redigida no fim do ano passado e reproduzido na íntegra a seguir:

Introdução
==========

A definição de *hackerspace*, segundo a Wikipédia, é:

> Um *hackerspace* \[\...\] é um local real (em oposição ao virtual) com
> o formato de um laboratório comunitário que segue a ética hacker,
> tendo espírito agregador, convergente e inspirador. Nele, pessoas com
> interesses em comum, normalmente em ciência, tecnologia, arte digital
> ou eletrônica podem se encontrar, socializar e colaborar. Um
> *hackerspace* pode ser visto como um laboratório comunitário, uma
> oficina ou um estúdio onde pessoas de diversas áreas podem trocar
> conhecimento e experiência para construir algo juntos.
>
> Muitos *hackerspaces* colaboram na divulgação e no desenvolvimento de
> software livre, *open hardware* e mídia alternativa e podem ser
> encontrados em *infoshops*, centros sociais e espaço comunitário.

A Faculdade de Engenharia Elétrica e de Computação (FEEC) da Unicamp
possui grande renome dentre as faculdades do mundo e principalmente na
América Latina, concentrando uma grande quantidade de alunos competentes
das áreas de elétrica e de computação. No entanto, devido à falta de um
espaço da natureza do *hackerspace*, estes alunos não têm a liberdade e
incentivo para aplicar os conhecimentos de sua área de uma maneira
criativa em projetos que poderiam inclusive resolver problemas do
cotidiano dos alunos.

Este documento visa possiblitar a criação de um *hackerspace* na FEEC.

Objetivos
=========

Os três objetivos centrais da criação do *hackerspace* na FEEC são:

-   Fornecer o espaço e equipamentos necessários para possibilitar o
    desenvolvimento de projetos e protótipos pelos alunos, realizados
    individualmente ou em grupo.

-   Fornecer um espaço que incentive a troca de conhecimentos entre
    alunos, permitindo que possam aprender uns com os outros,
    facilitando o desenvolvimento de projetos em áreas em que não se tem
    conhecimento prévio.

-   Garantir o livre acesso ao espaço, com o mínimo de burocracia
    possível, de forma a estimular a participação dos alunos nas
    atividades desenvolvidas dentro do *hackerspace*.

Motivação
=========

Na FEEC, grande parte dos assuntos estudados são abordados em duas
disciplinas, uma teórica e outra prática. As disciplinas práticas
ocorrem em laboratórios e seguem roteiros pré-estabelecidos. Assim, em
cada experimento, o aluno tem a oportunidade de experienciar, por meio
de aspectos práticos, aquilo que aprendeu na disciplina teórica.

A desvantagem dessa metodologia é que a necessidade de se realizar
ensaios seguindo um roteiro, faz com que o aluno aprenda apenas aquilo
que o professor julgou serem os pontos mais relevantes, limitando assim
a liberdade para inovar e compartilhar conhecimentos com os demais
alunos.

Por esse motivo, é importante existir um espaço em que o aluno possa
experimentar todos os outros aspectos que não couberam no roteiro do
laboratório.

Requisitos
==========

O ponto inicial e indispensável para a criação do *hackerspace* é uma
sala de livre acesso (ao contrário do LE23) e munida de cadeiras,
bancadas e lousas, suprindo assim dois dos objetivos básicos que são o
livre acesso e o incentivo à troca de conhecimentos entre os alunos.

O objetivo restante --- possibilitar o desenvolvimento de projetos e
protótipos --- pode ser suprido pela disponibilidade de equipamentos
como ferro de solda, multímetro e osciloscópio para projetos voltados
para engenharia elétrica, e computadores (que poderiam ser notebooks dos
próprios alunos), para projetos voltados para engenharia da computação.

Para permitir o livre acesso ao espaço e ao mesmo tempo disponibilizar
equipamentos de alto custo, antecipa-se ser necessário uma forma de
controlar o uso de tais equipamentos dentro do espaço (por exemplo,
usando um armário com controle de acesso). Essa forma de controle de
equipamentos pode ser discutida entre alunos e coordenadores e pode,
ainda, ser um dos projetos iniciais desenvolvido no espaço.

Expectativas
============

A expectativa para o hackerspace é que ele seja um espaço de convivência
comum entre os alunos, e possa ser gerido pelos mesmos, possibilitando a
fácil organização de eventos, tais como minicursos e palestras, por
qualquer um que tenha interesse.

Espera-se, por fim, que o espaço possa se financiar por meio da ajuda de
colaboradores e da instituição.
