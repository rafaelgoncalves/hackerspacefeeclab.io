Title: Participe do Hacktoberfest e comece a contribuir para projetos de código aberto!
Date: 2020-09-22

Agora em outubro, o Github, em parceria com outras empresas, organizará o
[Hacktoberfest][1], um evento que ocorre todo ano em outubro para incentivar
tanto iniciantes quanto veteranos a contribuir para projetos de código aberto
que estejam hospedados no Github.

O evento consiste em incentivar que os participantes proponham modificações no código de algum (ou alguns) projeto
que esteja no Github.
Essa modificação é então avaliada pela pessoa que cuida do projeto, e se fizer
sentido para o projeto é aceita e se torna parte do código.
O objetivo é colaborar para tornar os programas melhores e mais completos, e se
você contribuir para um programa que usa, de quebra ainda torna o programa
melhor para si mesmo(a)!
Essas submissões de alterações de código são chamadas de "Pull Request"s na
plataforma. Os "Pull Requests" são semelhantes aos "Merge Requests" do gitlab, a ferramenta utilizada para [contribuir para este site]({filename}/articles/meta/contribuindo.md)!

Vale notar que se você não sabe, ou não quer, programar, também pode participar! É
muito comum que a documentação do projeto não esteja adequada ou completa, e
alterações na documentação são muito bem-vindas!

As informações exatas, junto com o botão para inscrição no evento, ainda serão
publicadas quando ele iniciar, em 1º de outubro, na [página do evento][1], mas
para ter uma ideia de como funciona, na edição do ano passado, ao submeter 4
Pull Requests para projetos, você ganhava uma camiseta e alguns adesivos!

Boas fontes de informação (em inglês) sobre o funcionamento do Git, Github e
como tudo isso se encaixa e como usar para contribuir são as seguintes: [An
Introduction to Open Source][2] e [Understanding the GitHub flow][3].

Um dos principais focos do evento é trazer novas pessoas para contribuir e por
isso grupos são formados ao redor do mundo para dar apoio a iniciantes.
Por isso te convidamos a entrar no [nosso grupo do Element/Telegram][4]!
Também vamos estar contribuindo e adoraremos poder tirar suas dúvidas e te
ajudar a contribuir!

[1]: https://hacktoberfest.digitalocean.com/
[2]: https://www.digitalocean.com/community/tutorial_series/an-introduction-to-open-source
[3]: https://guides.github.com/introduction/flow/
[4]: {filename}/articles/meta/grupos.md
